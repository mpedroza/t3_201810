package model.data_structures;

import java.util.Iterator;

import com.google.gson.JsonObject;

public class LinkedLists<T extends Comparable<T>> implements ILinkedList<T>{

	private Node<T> head;
	private Node<T> tail;
	private int size;
	private Node<T> current = head;


	public LinkedLists ()
	{
		head = null;
		tail = null;
		size = 0;
	}

	public void add(T element){

		if (isEmpty())
		{
			head = new Node(element, null, null);
			current = head;
			tail = head;
		}

		else 
		{
			Node add = new Node((T)element, null, tail);
			tail.setNext(add);
			tail = add;
		
		}

		size++;
	}


	public void delete(T element)
	{
		Node cur = head;
		Node curT = tail;
		
		if(cur.getElement() == element)
		{ 
			head = cur.getNext();
			head.setPrev(null);
			
		}
		
		while (cur.getNext()!= null){
			
			cur = cur.getNext();
			
			if(cur.getElement()== element)
			{ 
				if(cur != head && cur != tail)
				{
					cur.getPrev().setNext(cur.getNext());
					cur.getNext().setPrev(cur.getPrev());
				}

				else if (curT.getElement()== element)
				{
					tail = cur.getPrev();
					cur.getPrev().setNext(null);
				}

				size--;
			}
		}
	}

	public T get(T element)
	{
		
		Node<T> cur = head;
		Node<T> curT = tail;
		
		if(head != null && tail != null) {
			
		if(cur.getElement() == element)
		{ 
			return (T) cur.getElement();
		}
		
		if(curT.getElement() == element)
		{ 
			return (T) curT.getElement();
		}
		
		else while (cur.getNext()!= null)
		{
				
				
				if(cur.getElement() == element)
				{ 
					return (T) cur.getElement();
				}
				
				cur = cur.getNext();

		}
		
		if(curT.getElement() == null) {

		return null;
		}
		
		}
		
		return null;
	}

	public boolean isEmpty ()
	{
		return size == 0;
	}

	public int getSize()
	{
		return size;
	}

	public Object getFirst()
	{
		if( isEmpty() )
		{  
			return null;
		}else
		{
			return (Object) head.getElement();
		}
	}

	public T next (){

		if (isEmpty())
		{
			return null;
		}
		else
		{
			current = current.getNext();
			return (T) current.getElement();

		}

	}

	public T getCurrent(){

		return (T) current.getElement();
	}

	public void listing(){

		Node aux = null;

		if (!isEmpty()){


			if(current != head)
			{
				aux = head;
				head = head.getNext();
				current = aux;
			}
		}

	}

}


