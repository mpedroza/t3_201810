package model.data_structures;

	public class Node<T extends Comparable<T>> {
		
		 private T element;
		 private Node next;
		 private Node prev;
		 
		 public Node (T item, Node pNext, Node pPrev)
		 {
		  element = item;
		  next = pNext;
		  prev = pPrev;
		 }
		 
		 public T getElement ()
		 {
		  return element;
		 }
		 
		 public Node getNext ()
		 {
		  return next;
		 }
		 
		 public Node getPrev ()
		 {
		  return prev;
		 }
		 
		 public void setNext (Node pNext)
		 {
		  next = pNext;
		 }
		 
		 public void setPrev (Node pPrev)
		 {
		  prev = pPrev;
		 }
		 
		 public void addAfter(Node<T> pNode)
		 {
		  pNode.prev = this;
		  if(next != null)
		  {
		   next.prev = pNode;
		   pNode.next = next;
		  } 
		  next = pNode;
		 }
		 
		 public void addBefore(Node pNode)
		 {
		  pNode.next = this;
		  pNode.prev = prev;
		  if(prev != null)
		  {
		   prev.next = pNode;
		  }
		  prev = pNode;
		 }
		 
		 public void delete ()
		 {
		  if(prev != null && next != null)
		  {
		   prev.next = next;
		   next.prev = prev;
		  }
		  
		  else if(prev == null)
		  {
		   next.prev = null;
		  }
		  
		  else if(next == null)
		  {
		   prev.next = null;
		  }
		 }
	}
		


		
