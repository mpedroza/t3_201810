package model.logic;

import api.ITaxiTripsManager;
import controller.Controller;
import jdk.nashorn.internal.parser.JSONParser;
import model.vo.Taxi;
import model.data_structures.ILinkedList;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;


public class TaxiTripsManager implements ITaxiTripsManager {

	// TODO
	// Definition of data model 

	private Queue <Service> services_queue = null;
	private Stack <Service> services_stack = null;
	String serviceFile = "./data/taxi-trips-wrvz-psew-medium.json";

	//taxi id: 1f601da85fba049c4ca4363c0bad4e6a96a5f72fe0beace329d0db5542b724892aa9a869def673292002803067a24c31bee67f9011dfe7a3c57ac0fe9a709722


	public void loadServices (String serviceFile, String taxiId) {
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(serviceFile));
			Gson gson = new GsonBuilder().create();
			Service[] services = gson.fromJson(reader, Service[].class);
			services_queue = new Queue<Service>();
			services_stack = new Stack<Service>();
			for(int i = 0; i < services.length; i++)
			{	
				if (services[i].getTaxiId().equals(taxiId))
				{
					services_queue.enqueue(services[i]);
					services_stack.push(services[i]);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} 

	}

	@Override
	public int[] servicesInInverseOrder() 
	{
		int[] InInverseOrder = new int[2];
		try {
			SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
			Service service1 = services_stack.peek();
			if (service1 == null)
			{
				return InInverseOrder;
			}
			Date date1 = (Date) date.parse(service1.getTripStartTime());
			Service service2 = null;
			Date date2 = null;

			if(service1 != null) 
			{
				Iterator<Service> services = services_stack.iterator();
				InInverseOrder[0] = 1;
				while (services.hasNext())
				{
					if (service2 == null)
					{
						service2 = services.next();
						date2 = (Date) date.parse(service2.getTripStartTime());
					}
					else if (date1.after(date2))
					{
						InInverseOrder[0] = ++InInverseOrder[0];
						service1 = service2;
						date1 = date2;
						service2 = services.next();
						date2 = (Date) date.parse(service2.getTripStartTime());
					}
					else 
					{
						InInverseOrder[1] = ++InInverseOrder[1];
						service2 = services.next();
						date2 = (Date) date.parse(service2.getTripStartTime());
					}

				}
			}

		} catch (ParseException e) {

			e.printStackTrace();

		}
		return InInverseOrder;
	}
	@Override
	public int [] servicesInOrder() 
	{
		Service servicio1 = services_queue.peek();
		Service servicio2 = null;
		SimpleDateFormat date = null;

		int ordenados = 1;
		int desordenados = 0;

		Date dateServ1 = null;
		Date dateServ2 = null;

		int [] resultado = new int[2];

		date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		if (servicio1 != null) {
			try 
			{
				dateServ1 = (Date) date.parse(servicio1.getTripStartTime());
				Iterator<Service> services = services_queue.iterator();

				while(services.hasNext()) 
				{
					if(servicio2 == null) 
					{
						servicio2 = services.next();
						dateServ2 = (Date) date.parse(servicio2.getTripStartTime());
					}
					else if (dateServ1.before(dateServ2)) 
					{
						ordenados++;
						servicio1 = servicio2;
						dateServ1=dateServ2;
						servicio2 = services.next();
						dateServ2 = (Date) date.parse(servicio2.getTripStartTime());
					}
					else 
					{
						desordenados++;
						servicio2 = services.next();
						dateServ2 = (Date) date.parse(servicio2.getTripStartTime());
					}
				}
			}
			catch (ParseException e) {
				e.printStackTrace();
			}

		}
		resultado[0] = ordenados;
		resultado[1] = desordenados;
		return resultado;
	}

}
