package model.vo;

import java.sql.Date;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Representation of a Service object
 */
public class Service implements Comparable<Service> {
	private String trip_id;
	private String taxi_id;
	private int trip_seconds;
	private double trip_miles;
	private double trip_total;
	private int dropoff_community_area;
	private String company;
	private String trip_start_timestamp;
	
	/**
	 * @return id - Trip_id
	 */
	public String getTripId() {
		// TODO Auto-generated method stub
		return trip_id;
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		return taxi_id;
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return trip_seconds;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return trip_miles;
	}
	
	/**
	 * @return total - Total cost of the trip
	 */
	public double getTripTotal() {
		// TODO Auto-generated method stub
		return trip_total;
	}
	
	/**
	 * @return community area - Destination of the trip
	 */
	public int getTripCommunityArea() {
		// TODO Auto-generated method stub
		return dropoff_community_area;
	}

	/**
	 * @return company - Company of the taxi
	 */
	public String getTripCompany() {
		// TODO Auto-generated method stub
		return company;
	}
	
	public String getTripStartTime() {
		return trip_start_timestamp;
	}
	
	
	@Override
	public int compareTo(Service o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
	public String toString()
	{
		return ("Trip id: " + getTripId());
	}

//	JsonObject service = null;
//
//	public Service (JsonObject pservice) {
//		service = pservice;
//	}
//
//	/**
//	 * @return id - Trip_id
//	 */
//	public String getTripId() {
//		// TODO Auto-generated method stub
//		
//		return service.get("trip_id").getAsString();
//	}	
//	
//	/**
//	 * @return id - Taxi_id
//	 */
//	public String getTaxiId() {
//		// TODO Auto-generated method stub
//		return service.get("taxi_id").getAsString();
//	}	
//	
//	/**
//	 * @return time - Time of the trip in seconds.
//	 */
//	public int getTripSeconds() {
//		// TODO Auto-generated method stub
//		return service.get("trip_seconds").getAsInt();
//	}
//
//	/**
//	 * @return miles - Distance of the trip in miles.
//	 */
//	public double getTripMiles() {
//		// TODO Auto-generated method stub
//		return service.get("trip_miles").getAsDouble();
//	}
//	
//	/**
//	 * @return total - Total cost of the trip
//	 */
//	public double getTripTotal() {
//		// TODO Auto-generated method stub
//		return service.get("trip_total").getAsDouble();
//	}
//
//	@Override
//	public int compareTo(Service o) {
//		// TODO Auto-generated method stub
//		return Integer.parseInt(this.getTaxiId()) - Integer.parseInt(o.getTaxiId());
//	}
//
//	public double getTripStartTime() {
//		return service.get("trip_start_timestamp").getAsDouble();
//	}
}
