package model.vo;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * Representation of a taxi object
 */
public class Taxi implements Comparable<Taxi>{

	JsonObject taxi;
	
	public Taxi(JsonObject ptaxi) {
		taxi = ptaxi;
	}
	
	/**
	 * @return id - taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
		if(taxi.get("taxi_id") != null)
		return taxi.get("taxi_id").getAsString();
		else return null;
	}

	/**
	 * @return company
	 */
	public String getCompany() {
		// TODO Auto-generated method stub
		if(taxi.get("company") != null)
		return taxi.get("company").getAsString();
		else return null;
	}
	
	@Override
	public int compareTo(Taxi o) {
		// TODO Auto-generated method stub
		
		if(o != null)
		return this.getTaxiId().compareTo(o.getTaxiId());
	
		return 1;
	}	
	
}
